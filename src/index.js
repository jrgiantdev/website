import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
setTimeout(()=>{
    ReactDOM.render(<App />, document.getElementById('root'));
    registerServiceWorker();
}, 1000);


import React, { Component } from 'react';
import styled, {css} from 'styled-components';
import {polygon} from './style-polygon';
const DS = styled.div`
min-heigt:400px;
max-height:1100px;
height:90vh;
width:100%;
display:grid;
grid-template-columns: repeat(2, 1fr);
`;

const POLY = styled.div`
grid-column=1;
grid-row=1/1;
background-color:green;
${props => {
    let css = "";
    
    if (props.side){
        css += polydirection(props.side);
    }
    return css;
}}
`;
function polydirection(direction){
    let retValue ='';
    if (direction.toLowerCase() == "left") retValue = css`${polygon("topRightX", "70%")} &>div, &>p {margin-left:30%}`;
    else if (direction.toLowerCase() == "right") retValue = css`${polygon("topLeftX", "30%")} &>div, &>p { margin-right: 30 %}`;
    else if (direction.toLowerCase() == "up") retValue = css`${polygon("topRightY", "30%")} &>div, &>p { margin-top: 30%}`;
    else if (direction.toLowerCase() == "down") retValue = css`${polygon("bottomLeftY", "70%")}&>div, &>p { margin-bottom: 30%}`;
    return retValue;
}
export default class DiagSplit extends Component{
    constructor(props){
        super(props);
        this.props = props;
    }
   
    render(){
        let ret;
        if(this.props.side == "right" || this.props.side == "up"){
            ret = (
                <DS>
                    {this.props.children[0]}
                    <POLY side={this.props.side} width={this.props.width}>{this.props.children[1]}</POLY>
                </DS>
            )
        }
        else {
            ret = (
                <DS>
                    <POLY side={this.props.side} width={this.props.width}>{this.props.children[0]}</POLY>
                    {this.props.children[1]}
                </DS>
            )
        }
        return ret;
        
    }
}
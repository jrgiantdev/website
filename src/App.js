import React, { Component } from 'react';
import logo from './logo.svg';
import './scss/index.scss';
import DiagSplit from './components/diagSplit'
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          <DiagSplit side="right" width="50%">
            <div>Child 1</div>
            <div>Child 2</div>
          </DiagSplit>
        </p>
      </div>
    );
  }
}

export default App;
